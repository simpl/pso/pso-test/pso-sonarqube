# PSO-soarqube

## Tables of contents

- [About pso-soarqube](#about-pso-soarqube)
- [Technologies recommended](#technologies-recommended)
- [SonarQube server installation and setup](#sonar-server-setup)
- [Launch docker compose](#launch-docker-compose)
- [Run SonarQube checks for a Maven project](#sonar-maven-run)

## About PSO-testing <a name="about-pso-soarqube"></a>

The `pso-soarqube` is a GitLab repository dedicated for keeping the code and configuration needed when running `SonarQube` checks in a Docker containerized environment for a Maven project.

## Technologies recommended <a name="technologies-recommended"></a>
- Java 11
- SonarQube 9.9.5 community
- Postgres 16.3-alpine
- Maven 3.9.6
- Docker >= 26.1.3
- Docker compose >= 2.27.0

## SonarQube server installation and setup <a name="sonar-server-setup"></a>

For the server setup we need to follow the steps from the official documentation from SonarQube page. On this page we get all the information about how to perform the setup using a Docker image by running a Docker Compose .yml file.

- [Installing SonarQube from the Docker image](https://docs.sonarsource.com/sonarqube/latest/setup-and-upgrade/install-the-server/installing-sonarqube-from-docker/)

## Launch docker compose <a name="launch-docker-compose"></a>

1. First go to the location of the docker compose file:
```bash
cd <docker_compose_dir>
```

2. Run the command to launch the docker compose file:
```bash
docker compose up --build sonarqube -d
```

When the docker compose file is started and running, it will launch a local SonarQube server on 9000 port `http://localhost:9000/`, that will be used to run and show sonar result for a project.

See [this guide](https://www.baeldung.com/sonar-qube) for how to access the local SonarQube server and add your project to the server. 

**_NOTE:_** See also the [Pre-installation steps on Linux systems](https://docs.sonarsource.com/sonarqube/latest/setup-and-upgrade/pre-installation/linux/) that are required.

## Run SonarQube checks for a Maven project <a name="sonar-maven-run"></a>

Check first the [SonarScanner for Maven](https://docs.sonarsource.com/sonarqube/latest/analyzing-source-code/scanners/sonarscanner-for-maven/) documentation.
Once the Maven project is added to the SonarQube server, then the sonar quality checks can be run with a command from the terminal or in a script.

Run the following command:
```bash
mvn clean verify sonar:sonar -Dsonar.projectKey=PROJECT_KEY 
                             -Dsonar.projectName='PROJECT_NAME' 
                             -Dsonar.host.url=http://localhost:9000 
                             -Dsonar.token=THE_GENERATED_TOKEN
```

Use the specific project attributes in the above command to replace the `PROJECT_KEY`, `PROJECT_NAME` and `THE_GENERATED_TOKEN`.

**Example:**
For a demo purpose you can use the [sonarqube-maven-example](https://github.com/osandadeshan/sonarqube-maven-example) GitHub project to try out the SonarQube checks works in your environment.

In order to run the `mvn` command you need to be placed where the `pom.xml` file is located:
```bash
cd <project_pom_file_location>
```

Then use the following command to run sonar-scanner on the example project:
```bash
mvn clean verify sonar:sonar -Dsonar.projectKey=sonarqube-maven-example 
                             -Dsonar.projectName='sonarqube-maven-example' 
                             -Dsonar.host.url=http://localhost:9000 
                             -Dsonar.token=sqp_b8c16cd3ad4d9c41375090ed20231b0d252ef41d
```

After running the `mvn` command, connect to the SonarQube local server and see the report for the example project in the `QUALITY GATE STATUS`.
